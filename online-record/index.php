<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 09.08.2018
 * Time: 14:45
 */
/**
 * Plugin Name: Online Record
 * Description: This is plugin for a form Online Record with admin side
 * Author: Andrew Puzirevich
 * Version: 1.0
 */


function onlineRecord() {

	global $wpdb;

	if ( isset( $_POST['save'] ) ) {
		$id = $_POST['id'];
		$wpdb->update(
			'wp_online_records',
			array( 'is_confirm' => true ),
			array( 'id' => $id )
		);
	}
	if ( isset( $_POST['del'] ) ) {
		$id = $_POST['id'];
		$wpdb->delete(
			'wp_online_records',
			array( 'id' => $id )
		);

	}

	$query = "SELECT * FROM `wp_online_records` WHERE is_confirm=0";
	$list  = $wpdb->get_results( $query, OBJECT );

	$count  = count( $list );
	$pageId = 1;
	if ( isset( $_POST['page'] ) ) {
		$pageId = $_POST['page'];
	}
	$pageCount = (int) ceil( count( $list ) / 15 );
	if ( isset( $_POST['prev'] ) && $pageId != 1 ) {
		$pageId = $pageId - 1;
	} elseif ( isset( $_POST['next'] ) && $pageId < $pageCount ) {
		$pageId = $pageId + 1;
	} else {
		$pageId = 1;
	}


	$to   = 15 * $pageId;
	$from = $to - 15;
	if ( $to > $count ) {
		$to = $count;
	}
	?>
    <head>
        <link rel="stylesheet" media="screen" href="<?= plugins_url( 'online-record/style.css' ) ?>">
    </head>
    <body>
    <div class="full-width" ">
        <h3>Форма нерасмотренных заявок</h3>
        <p>*при отклонении, заявка удаляется</p>
        <table class="orders" cellspacing="0">
            <tr>
                <th># Заявки</th>
                <th>ФИО</th>
                <th>Телефон</th>
                <th>Почта</th>
                <th>Услуга</th>
                <th>Примечание</th>
                <th>Когда пришла заявка</th>
                <th>Подтверждение</th>
            </tr>

			<?php for ( $i = $from; $i < $to; $i ++ ) { ?>
                <form method="post" id="orders-form">
                    <tr>
                        <td><?php echo $list[ $i ]->id ?></td>
                        <td style="max-width: 200px;"><p><?php echo $list[ $i ]->fio ?></p></td>
                        <td><p><?php echo $list[ $i ]->tel ?></p></td>
                        <td><p><?php echo $list[ $i ]->mail ?></p></td>
                        <td><?php
							$idServ  = $list[ $i ]->id_service;
							$query   = "SELECT name FROM `wp_services` WHERE id='$idServ'";
							$service = $wpdb->get_row( $query, OBJECT );
							echo $service->name;
							?></td>
                        <td style="max-width: 200px;"><p style="max-height: 220px; overflow-y: auto;word-break: break-all;"><?php echo $list[ $i ]->note ?></p></td>
                        <td><?php $d = $list[ $i ]->date_of_issue;
	                        $new_date = date('d.m.Y H:i:s', strtotime("+3 hours", strtotime($d)));
                            echo $new_date ?></td>
                        <td>
                            <button class="appr" name="save">Подтвердить</button>
                            <?php
                            $current_user = get_userdata( get_current_user_id() );
                            $roles        = (array) $current_user->roles;
                            if (in_array('administrator',$roles)){
                                echo '<button class="del" name="del">Отклонить</button>';
                            }
                            ?>

                            <input type="hidden" name="id" value="<?php echo $list[ $i ]->id ?>">
                        </td>
                    </tr>
                </form>
				<?php
			}
			?>

        </table>
        <form method="POST">
            <input type="hidden" value="<?php echo $pageId; ?>" name="page" id="page">
            <div class="navigation">
				<?php if ( $pageId > 1 ) :
					echo '<input type="submit" name="prev" id="prev" value="<" style="margin: 10px">';
				endif;
				echo '<span>Страница ' . $pageId . ' из ' . $pageCount . '</span>';
				if ( $pageId < $pageCount ):
					echo '<input type="submit" name="next" id="next" value=">" style="margin: 10px"> ';
				endif;
				?>
            </div>
        </form>
    </div>
    </body>
	<?php

}

function approvedOrders() {
	global $wpdb;
	$query = "SELECT * FROM `wp_online_records` WHERE is_confirm=1 ORDER BY date_of_issue DESC";
	$list  = $wpdb->get_results( $query, OBJECT );

	if (isset($_POST['filter'])){
	    $fio = $_POST['filterFio'];
	    $timeFrom = $_POST['timeFrom'];
	    $timeTo = $_POST['timeTo'];

	    if ($fio != '' && $timeFrom == '' && $timeTo == ''){
		    $queryFilter = "SELECT * FROM `wp_online_records` WHERE is_confirm=1 AND fio LIKE '%$fio%'";
            $list = $wpdb->get_results($queryFilter,OBJECT);
        }
        if ($fio == '' && $timeFrom != '' && $timeTo != ''){
            $queryFilter = "SELECT * FROM `wp_online_records` WHERE is_confirm=1 AND date_of_issue BETWEEN '$timeFrom' AND '$timeTo'";
            $list = $wpdb->get_results($queryFilter, OBJECT);
        }
        if ($fio != '' && $timeFrom != '' && $timeTo != ''){
	        $queryFilter = "SELECT * FROM `wp_online_records` WHERE is_confirm=1 AND fio LIKE '%$fio%' AND date_of_issue BETWEEN '$timeFrom' AND '$timeTo'";
	        $list = $wpdb->get_results($queryFilter, OBJECT);
        }
		if ($fio != '' && $timeFrom != '' && $timeTo == ''){
			$queryFilter = "SELECT * FROM `wp_online_records` WHERE is_confirm=1 AND fio LIKE '%$fio%' AND date_of_issue>='$timeFrom'";
			$list = $wpdb->get_results($queryFilter, OBJECT);
		}
		if ($fio != '' && $timeFrom == '' && $timeTo != ''){
			$queryFilter = "SELECT * FROM `wp_online_records` WHERE is_confirm=1 AND fio LIKE '%$fio%' AND date_of_issue<='$timeTo'";
			$list = $wpdb->get_results($queryFilter, OBJECT);
		}
		if ($timeFrom != '' && $timeTo != '' && $timeFrom == $timeTo){
			$queryFilter = "SELECT * FROM `wp_online_records` WHERE is_confirm=1 AND date_of_issue LIKE '%$timeFrom%'";
			$list = $wpdb->get_results($queryFilter,OBJECT);
        }
    }
    if (isset($_POST['cancel'])){
// TO DO
    }
	?>
    <head>
        <link rel="stylesheet" media="screen" href="<?= plugins_url( 'online-record/style.css' ) ?>">
    </head>
    <body>
    <div class="full-width">
        <h3>Все принятые заявки</h3>
        <form method="post">
        <p>Фильтровать по: ФИО: <input type="text" name="filterFio" value="<?php echo $_POST["filterFio"]; ?>"> и/или, по времени заказа, от <input type="date" name="timeFrom" value="<?php echo $_POST["timeFrom"]; ?>">, до <input type="date" name="timeTo" value="<?php echo $_POST["timeTo"]; ?>">
        <button type="submit" name="filter" class="appr">Фильтровать</button><button type="submit" name="cancel" class="del">Сбросить фильтр</button></p>
        </form>
        <table class="orders" cellspacing="0">
            <tr>
                <th># Заявки</th>
                <th>ФИО</th>
                <th>Телефон</th>
                <th>Почта</th>
                <th>Услуга</th>
                <th>Примечание</th>
                <th>Когда пришла заявка</th>
            </tr>

			<?php
			$count  = count( $list );
			$pageId = 1;
			if ( isset( $_POST['page'] ) ) {
				$pageId = $_POST['page'];
			}
			$pageCount = (int) ceil( count( $list ) / 15 );
			if ( isset( $_POST['prev'] ) && $pageId != 1 ) {
				$pageId = $pageId - 1;
			} elseif ( isset( $_POST['next'] ) && $pageId < $pageCount ) {
				$pageId = $pageId + 1;
			} else {
				$pageId = 1;
			}


			$to   = 15 * $pageId;
			$from = $to - 15;
			if ( $to > $count ) {
				$to = $count;
			}

			?>

			<?php for ( $i = $from; $i < $to; $i ++ ) { ?>

                <tr>
                    <td><?php echo $list[ $i ]->id ?></td>
                    <td style="max-width: 200px;"><p><?php echo $list[ $i ]->fio ?></p></td>
                    <td><p><?php echo $list[ $i ]->tel ?></p></td>
                    <td><p><?php echo $list[ $i ]->mail ?></p></td>
                    <td><?php
						$idS     = $list[ $i ]->id_service;
						$query   = "SELECT name FROM `wp_services` WHERE id='$idS'";
						$service = $wpdb->get_row( $query, OBJECT );
						echo $service->name;
						?></td>
                    <td style="max-width: 200px;"><p style="max-height: 220px; overflow-y: auto;word-break: break-all;"><?php echo $list[ $i ]->note ?></p></td>
                    <td><?php $d = $list[ $i ]->date_of_issue;
	                    $new_date = date('d.m.Y H:i:s', strtotime("+3 hours", strtotime($d)));
	                    echo $new_date ?></td>
                </tr>

				<?php
			}
			?>

        </table>
        <form method="POST">
            <input type="hidden" value="<?php echo $pageId; ?>" name="page" id="page">
            <div class="navigation">
				<?php if ( $pageId > 1 ) :
					echo '<input type="submit" name="prev" id="prev" value="<" style="margin: 10px">';
				endif;
				echo '<span>Страница ' . $pageId . ' из ' . $pageCount . '</span>';
				if ( $pageId < $pageCount ):
					echo '<input type="submit" name="next" id="next" value=">" style="margin: 10px"> ';
				endif;
				?>
            </div>
        </form>
    </div>
    </body>
	<?php
}


function register_my_orders_menu_page() {
	add_menu_page( 'Заявки на рассмотрение', 'Заявки на рассмотрение', 'manage_options', 'records', 'onlineRecord', '' );
	add_submenu_page( 'records', 'Принятые заявки', 'Принятые заявки', 'manage_options', 'approved', 'approvedOrders' );
}

add_action( 'admin_menu', 'register_my_orders_menu_page' );