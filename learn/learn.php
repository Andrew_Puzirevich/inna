<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 23.08.2018
 * Time: 11:31
 */
/**
 * Plugin Name: Administrate learning courses
 * Description: This is plugin for adding courses and checking it for showing in front
 * Author: Andrew Puzirevich
 * Version: 1.0
 */

function learnMod()
{
	global $wpdb;
	$isS = false;
	if ( isset( $_POST['saveNew'] ) ) {
		if ( $_POST['isShow'] == "on" ) {
			$isS = true;
		}
		$nameU = $_POST['nameUk'];
		$nameR = $_POST['nameRu'];
		$pageU = $_POST['pageUk'];
		$pageR = $_POST['pageRu'];
		$query = "INSERT INTO `wp_learn` (name_course_uk,name_course,page_uk,page_ru,show_table) VALUES ('$nameU', '$nameR', '$pageU', '$pageR','$isS')";
		$wpdb->query( $query );
	}
	if ( isset( $_POST['update'] ) ) {
		if ( $_POST['isShow'] == "on" ) {
			$isS = true;
		}
		$nameU = $_POST['nameUk'];
		$nameR = $_POST['nameRu'];
		$pageU = $_POST['pageUk'];
		$pageR = $_POST['pageRu'];
		$id    = (int)$_POST['id'];
		$wpdb->update(
			'wp_learn',
			array(
				'name_course_uk' => $nameU,
				'name_course'    => $nameR,
				'page_uk'        => $pageU,
				'page_ru'        => $pageR,
				'show_table'     => $isS
			),
			array( 'id' => $id )
		);
	}
	if (isset($_POST['del'])){
		$id = $_POST['id'];
		$wpdb->delete( 'wp_learn', array( 'id' => $id ) );
    }
	?>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        function SendPost(el) {
            var del = confirm("Вы действительно хотите удалить этот курс?");
            if (del) {
                var id = el.id.split('-')[1];
                $.ajax({
                    type: 'post',
                    url: '',
                    data: {'delConfirm': 'true', 'id': id},
                    response: 'text',
                    success: function () {
                        setTimeout(
                            function () {
                                location.href = "../wp-admin/admin.php?page=learn";
                            }
                            , 1000);
                    }
                });
            }
        }
    </script>
	<link rel="stylesheet" media="screen" href="<?= plugins_url( 'learn/style.css' ) ?>">
</head>
	<div style="width: 99%; display: inline-block">
		<div style="width: 100%; margin-top: 20px">
			<h3>Добавить категорию</h3>
			<table cellspacing="0" class="learn">
				<tr>
					<th>Название УКР</th>
					<th>Название РУС</th>
					<th>Страница УКР</th>
					<th>Страница РУС</th>
					<th>Показать в Обучении</th>
					<th>Подтверждение</th>
				</tr>
				<form method="post">
				<tr>
					<td><input type="text" name="nameUk"></td>
					<td><input type="text" name="nameRu"></td>
					<td><select name="pageUk" required>
							<option value=""></option>
							<?php
							$pages = get_pages( array( 'meta_key'     => '_wp_page_template',
							                           'meta_value'   => 'learn-template-uk.php',
							                           'hierarchical' => 0
							) );
							foreach ( $pages as $page ) {
								$option = '<option value="/' . get_page_uri( $page->ID ) . '">';
								$option .= $page->post_title;
								$option .= '</option>';
								echo $option;
							}
							?>
						</select>
					</td>
					<td><select name="pageRu" required>
							<option value=""></option>
							<?php
							$pages = get_pages( array( 'meta_key'     => '_wp_page_template',
							                           'meta_value'   => 'learn-template.php',
							                           'hierarchical' => 0
							) );
							foreach ( $pages as $page ) {
								$option = '<option value="/' . get_page_uri( $page->ID ) . '">';
								$option .= $page->post_title;
								$option .= '</option>';
								echo $option;
							}
							?>
						</select></td>
					<td><input type="checkbox" name="isShow"></td>
					<td> <button class="appr" name="saveNew" type="submit">Сохранить</button></td>
				</tr></form>
			</table>
		</div>
	</div>

	<div style="width: 100%;  margin-top: 20px">
		<h3>Доступные услуги на сайте</h3>
		<table cellspacing="0" class="learn">
			<tr>
				<th>#</th>
				<th>Название УКР</th>
				<th>Название РУС</th>
				<th>Страница (УКР)</th>
				<th>Cтраницa (РУС)</th>
				<th style="max-width: 100px">Показать в Обучении</th>
				<th>Подтверждение</th>
			</tr>
			<?php

			$query = "SELECT * FROM `wp_learn`";
			$courses = $wpdb->get_results($query,OBJECT);
			$num = 1;
			foreach ( $courses as $course ) {
				?>
				<form method="post">
					<tr>
						<td><?php echo $num; $num++; ?></td>
						<td style="min-width: 200px"><input type="text" name="nameUk" value="<?php echo $course->name_course_uk ?>"></td>
						<td style="min-width: 200px"><input type="text" name="nameRu" value="<?php echo $course->name_course ?>"></td>


						<td>
							<select name="pageUk">
								<option value=""></option>
								<?php
								$pages = get_pages( array( 'meta_key'     => '_wp_page_template',
								                           'meta_value'   => 'learn-template-uk.php',
								                           'hierarchical' => 0
								) );
								foreach ( $pages as $page ) {
									if ( '/'.get_page_uri( $page->ID ) == $course->page_uk ) {
										$option = '<option value="/' . get_page_uri( $page->ID ) . '" selected="selected">';
									} else {
										$option = '<option value="/' . get_page_uri( $page->ID ) . '">';
									}
									$option .= $page->post_title;
									$option .= '</option>';
									echo $option;
								}
								?>
							</select>
						</td>
						<td>
							<select name="pageRu">
								<option value=""></option>
								<?php
								$pages = get_pages( array( 'meta_key'     => '_wp_page_template',
								                           'meta_value'   => 'learn-template.php',
								                           'hierarchical' => 0
								) );
								foreach ( $pages as $page ) {
									if ( '/'.get_page_uri( $page->ID ) == $course->page_ru) {
										$option = '<option value="/' . get_page_uri( $page->ID ) . '" selected="selected">';
									} else {
										$option = '<option value="/' . get_page_uri( $page->ID ) . '">';
									}
									$option .= $page->post_title;
									$option .= '</option>';
									echo $option;
								}
								?>
							</select>
						</td>
						<td style="max-width: 100px"><input type="checkbox"
						                                    name="isShow" <?php if ( $course->show_table == true ) {
								echo "checked = 'checked'";
							}
							?>></td>

						<td>
							<input type="hidden" name="id" value="<?php echo $course->id ?>">
							<button class="appr" name="update" type="submit">Обновить</button>
							<?php
							$current_user = get_userdata( get_current_user_id() );
							$roles        = (array) $current_user->roles;
							if (in_array('administrator',$roles)){?>
								<button class="del" name="del" id="del-<?php echo $course->id ?>" type="submit"
								        onclick="SendPost(this)">Удалить</button>
							<?php }
							?>

						</td>
					</tr>
				</form>
			<?php } ?>
		</table>
	</div>

	<?php
}

function register_my_learn_menu_page() {
	add_menu_page( 'Категории обучения', 'Категории обучения', 'manage_options', 'learn', 'learnMod', '' );
}

add_action( 'admin_menu', 'register_my_learn_menu_page' );